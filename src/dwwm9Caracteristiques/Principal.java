package dwwm9Caracteristiques;

import java.text.DecimalFormat;

import tools.Saisie;

public class Principal {
	private static final String MSG_NOM = "Merci d'indiquer votre nom ";
	private static final String MSG_PRENOM = "Merci d'indiquer votre prénom ";
	private static final String MSG_AGE = "Merci d'indiquer votre âge ";
	private static final String MSG_TAILLE = "Merci d'indiquer votre taille ";

	public static void main(String[] args) {

		String xnom = Saisie.saisirChaine(MSG_NOM, 1, 20);
		String xprenom = Saisie.saisirChaine(MSG_PRENOM, 1, 15);
		int xage = Saisie.saisirEntier(MSG_AGE, 1, 120);
		double xtaille = Saisie.saisirReel(MSG_TAILLE, 1.0, 2.1);

		DecimalFormat f = new DecimalFormat(); // Formatter en décimal
		f.setMaximumFractionDigits(2); // Autoriser 2 chiffres après la virgule

		System.out.println("Bonjour " + xprenom + " " + xnom + "\nVous avez " + xage + " ans\n" + "Vous mesurez "
				+ f.format(xtaille) + " mètre");
		System.out.println(
				(xage >= 18) ? "Vous êtes majeur" : (xage < 12) ? "Vous êtes un enfant" : "Vous êtes un adolescent");
		// autre solution
		//
		// String texte="";
		// if (xage>=18) {
		// texte = "vous etes majeur";
		// }else if(xage<12) {
		// texte = "vous etes un enfant";
		// }else {
		// texte = "vous etes un adolescent";
		// }
		// System.out.print(texte);
	}
}
