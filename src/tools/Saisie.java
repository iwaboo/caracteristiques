package tools;

import java.util.Scanner;

public class Saisie {
	static Scanner scan = new Scanner(System.in);

	public static String saisirChaine(String message, int min, int max) {

		String info;
		int cpt = 0;

		do {
			if (cpt > 0) {
				System.err.println("Erreur");
			}
			System.out.print(message + "entre " + min + " et " + max + " caractères : ");
			info = scan.nextLine();
			cpt++;

		} while (info.length() < min || info.length() > max);

		return info;
	}

	public static int saisirEntier(String message, int min, int max) {

		int entier = 0;
		int cpt = 0;

		do {
			if (cpt > 0) {
				System.err.println("Erreur");
			}
			System.out.println(message + "entre " + min + " et " + max + " : ");
			try {
				entier = scan.nextInt();
			} catch (Exception e) {
				System.err.println("Erreur : Veuillez saisir un entier");
				scan.nextLine();
			}

			cpt++;

		} while (entier < min || entier > max);

		return entier;
	}

	public static double saisirReel(String message, double min, double max) {
		double reel = 0;
		int cpt = 0;

		do {
			if (cpt > 0) {
				System.err.println("Erreur");
			}
			System.out.println(message + "entre " + min + " et " + max + " : ");
			try {
				reel = scan.nextDouble();
			} catch (Exception e) {
				System.err.println("Erreur : Veuillez saisir un décimal");
				scan.nextLine();
			}

			cpt++;

		} while (reel < min || reel > max);

		return reel;
	}
}
